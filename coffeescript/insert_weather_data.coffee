require('../../util/javascript/arrayUtils').install()
Promise = require 'promise'
getWeather = require './openweathermap'
createMeasureTemplate = require '../../util/javascript/createMeasureTemplate'
saveMeasure = require '../../util/javascript/saveMeasure'
options = require('minimist')(process.argv,{boolean:['saveMeasures','debug'],default:{saveMeasures:true,debug:false}})

config = require '../config'
dbDriver = require '../../util/javascript/dbDriver'
configDb = dbDriver.connectToServer(config).useDb(config.name + "_config")
dataDb = dbDriver.connectToServer(config).useDb(config.name + "_data")

configDb.view 'stand/location',{include_docs:true}
.then (_locations)->
    
    locs = {}
    for _loc in _locations
        
        locs[_loc.doc._id] = _loc.doc
    
    locations = (loc for own _,loc of locs)

    for location in locations
        
        getWeather location,options
        .then (data)->
            
            if options.debug
                console.log data

            measure = createMeasureTemplate()
            measure.measureOrigin = "external"
            measure.source = 'openweathermap'
            measure.name = 'weather'
            measure.raw_value = data
            measure.value = data
            measure.units = 'metric'
            measure.location_id = location._id
            
            if options.saveMeasures
                if options.debug
                    console.log "saving measure..."
                saveMeasure(measure,dataDb,false)
                .then ->
                    if options.debug
                        console.log "openweathermap weather data uploaded to db " + config.database.name
.catch (err)->
    console.log err
