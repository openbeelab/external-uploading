util = require 'util'
Promise = require 'promise'
config = require '../config'
http = require 'http'
#http = {}
#http.get = Promise.denodeify(http2.get.bind(http2))
key = config.openweathermap.key

module.exports = (location,options) ->
    
    #lat=44.849264
    #lon=-0.572434
    url = "http://api.openweathermap.org/data/2.5/weather?lat=" + location.latitude + "&lon=" + location.longitude + "&mode=json&units=metric&appid="+key

    weatherPromise = new Promise((resolve,reject)->
        http.get url, (response)->
        
            str = ''

            #another chunk of data has been recieved, so append it to `str`
            response.on 'data', (chunk)->
                str += chunk

            #the whole response has been recieved, so we just print it out here
            response.on 'end', ->
            
                try
                    if options.debug
                        console.log "location:" + location.name
                        console.log "url:" + url
                    
                    data = JSON.parse(str)
                    if options.debug
                        console.log "data:" + util.inspect(data)
                    
                    ret = {}
                    ret['temp'] = data.main.temp
                    ret['pressure'] = data.main.pressure
                    ret['humidity'] = data.main.humidity
                    ret['wind']  = data.wind

                    resolve(ret)
                catch e
                    reject e
    )
    return weatherPromise
